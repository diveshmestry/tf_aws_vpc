#--------------------------------------------------------------
# Routing table for App subnets
#--------------------------------------------------------------
resource "aws_route_table" "app_route_table" {
  count            = length(var.app_subnets)
  vpc_id           = aws_vpc.app_vpc.id

  tags = {
    Name        = "App route table ${count.index}"
    environment = var.vpc_environment_tag
    product     = var.vpc_product_tag
    contact     = var.vpc_contact_tag
    role        = "App Routing Table"
  }
}

#--------------------------------------------------------------
# Route for App routing table
#--------------------------------------------------------------
resource "aws_route" "app_route" {
  count                  = length(var.app_subnets)
  route_table_id         = element(aws_route_table.app_route_table.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.nat.*.id, count.index)
}

#--------------------------------------------------------------
# Association between app subnets and route tables
#--------------------------------------------------------------
resource "aws_route_table_association" "app_rt_assoc" {
  count          = length(var.app_subnets)
  subnet_id      = element(aws_subnet.app_subnets.*.id, count.index)
  route_table_id = element(aws_route_table.app_route_table.*.id, count.index)
}

#--------------------------------------------------------------
# Routing table for public subnets
#--------------------------------------------------------------
resource "aws_route_table" "public_route_table" {
  count            = length(var.public_subnets)
  vpc_id           = aws_vpc.app_vpc.id

  tags = {
    Name        = "Public route table ${count.index}"
    environment = var.vpc_environment_tag
    product     = var.vpc_product_tag
    contact     = var.vpc_contact_tag
    role        = "Public Routing Table"
  }
}

#--------------------------------------------------------------
# Route for public routing table
#--------------------------------------------------------------
resource "aws_route" "public_route" {
  count                  = length(var.public_subnets)
  route_table_id         = element(aws_route_table.public_route_table.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id
}

#--------------------------------------------------------------
# Association between public subnets and route tables
#--------------------------------------------------------------
resource "aws_route_table_association" "public_rt_assoc" {
  count          = length(var.public_subnets)
  subnet_id      = element(aws_subnet.public_subnets.*.id, count.index)
  route_table_id = element(aws_route_table.public_route_table.*.id, count.index)
}

#--------------------------------------------------------------
# Routing table for data subnets
#--------------------------------------------------------------
resource "aws_route_table" "data_route_table" {
  count            = length(var.data_subnets)
  vpc_id           = aws_vpc.app_vpc.id

  tags = {
    Name        = "Data route table ${count.index}"
    environment = var.vpc_environment_tag
    product     = var.vpc_product_tag
    contact     = var.vpc_contact_tag
    role        = "Data Routing Table"
  }
}

#--------------------------------------------------------------
# Route for data routing table
#--------------------------------------------------------------
resource "aws_route" "data_route" {
  count                  = length(var.data_subnets)
  route_table_id         = element(aws_route_table.data_route_table.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.nat.*.id, count.index)
}

#--------------------------------------------------------------
# Association between data subnets and route tables
#--------------------------------------------------------------
resource "aws_route_table_association" "data_rt_assoc" {
  count          = length(var.data_subnets)
  subnet_id      = element(aws_subnet.data_subnets.*.id, count.index)
  route_table_id = element(aws_route_table.data_route_table.*.id, count.index)
}