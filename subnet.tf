#--------------------------------------------------------------
# Public subnet resource definition
#--------------------------------------------------------------
resource "aws_subnet" "public_subnets" {
  count                   = length(var.public_subnets)
  vpc_id                  = aws_vpc.app_vpc.id
  cidr_block              = element(var.public_subnets, count.index)
  availability_zone       = element(var.availability_zones, count.index)
  map_public_ip_on_launch = true

  tags = {
    Name        = "${var.vpc_name}.public.${element(var.availability_zones,count.index)}"
    environment = var.vpc_environment_tag
    role        = "public_subnet"
    product     = var.vpc_product_tag
    contact     = var.vpc_contact_tag
  }

  lifecycle {
    create_before_destroy = true
  }
}

#--------------------------------------------------------------
# Private subnet resource definition
#--------------------------------------------------------------
resource "aws_subnet" "app_subnets" {
  count                   = length(var.app_subnets)
  vpc_id                  = aws_vpc.app_vpc.id
  cidr_block              = element(var.app_subnets, count.index)
  availability_zone       = element(var.availability_zones, count.index)
  map_public_ip_on_launch = false

  tags = {
    Name        = "${var.vpc_name}.private.${element(var.availability_zones,count.index)}"
    environment = var.vpc_environment_tag
    role        = "private_subnet"
    product     = var.vpc_product_tag
    contact     = var.vpc_contact_tag
  }

  lifecycle {
    create_before_destroy = true
  }
}

#--------------------------------------------------------------
# Private subnet resource definition
#--------------------------------------------------------------

resource "aws_subnet" "data_subnets" {
  count                   = length(var.data_subnets)
  vpc_id                  = aws_vpc.app_vpc.id
  cidr_block              = element(var.data_subnets, count.index)
  availability_zone       = element(var.availability_zones, count.index)
  map_public_ip_on_launch = false

  tags = {
    Name        = "${var.vpc_name}.data.${element(var.availability_zones,count.index)}"
    environment = var.vpc_environment_tag
    role        = "data_subnet"
    product     = var.vpc_product_tag
    contact     = var.vpc_contact_tag
  }

  lifecycle {
    create_before_destroy = true
  }
}
