#--------------------------------------------------------------
# App Subnet Network ACL
#--------------------------------------------------------------
resource "aws_network_acl" "app" {
  vpc_id     = aws_vpc.app_vpc.id
  subnet_ids = flatten([aws_subnet.app_subnets.*.id])

  tags = {
    Name        = "app-acl"
    environment = var.vpc_environment_tag
    product     = var.vpc_product_tag
    contact     = var.vpc_contact_tag
    role        = "Private Subnet ACL"
  }
}

# Authorize all inbound traffic.
resource "aws_network_acl_rule" "allow_ingress_all" {
  egress         = false
  protocol       = "all"
  rule_number    = 100
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 0
  to_port        = 0
  network_acl_id = aws_network_acl.app.id
}

# Authorize all outbound traffic.
resource "aws_network_acl_rule" "app_allow_egress_all" {
  egress         = true
  protocol       = "all"
  rule_number    = 100
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 0
  to_port        = 0
  network_acl_id = aws_network_acl.app.id
}

#--------------------------------------------------------------
# Public Subnet Network ACL
#--------------------------------------------------------------
resource "aws_network_acl" "public" {
  vpc_id     = aws_vpc.app_vpc.id
  subnet_ids = flatten([aws_subnet.public_subnets.*.id])

  tags = {
    Name        = "public-acl"
    environment = var.vpc_environment_tag
    product     = var.vpc_product_tag
    contact     = var.vpc_contact_tag
    role        = "Public Subnet ACL"
  }
}

/*#--------------------------------------------------------------
# Public Subnet Network ACL - Ingress Rules
#--------------------------------------------------------------
resource "aws_network_acl_rule" "allow_ingress_smtps" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 10
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 465
  to_port        = 465
}*/

resource "aws_network_acl_rule" "public_allow_ingress_all" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 100
  egress         = false
  protocol       = "all"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
}

resource "aws_network_acl_rule" "allow_ingress_http" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 110
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 80
  to_port        = 80
}

resource "aws_network_acl_rule" "allow_ingress_http_everywhere_ipv6" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 115
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  ipv6_cidr_block = "::/0"
  from_port      = 80
  to_port        = 80
}

resource "aws_network_acl_rule" "allow_ingress_https" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 120
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 443
  to_port        = 443
}

resource "aws_network_acl_rule" "allow_ingress_https_everywhere_ipv6" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 125
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  ipv6_cidr_block = "::/0"
  from_port      = 443
  to_port        = 443
}



/*resource "aws_network_acl_rule" "allow_ingress_icmp" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 300
  egress         = false
  protocol       = "icmp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = -1
  to_port        = -1
  icmp_type      = -1
  icmp_code      = -1
}*/

/*resource "aws_network_acl_rule" "allow_ingress_ntp" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 350
  egress         = false
  protocol       = "udp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 123
  to_port        = 123
}*/

/*resource "aws_network_acl_rule" "allow_ingress_ntp_ephemeral" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 360
  egress         = false
  protocol       = "udp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 1024
  to_port        = 65535
}*/

/*resource "aws_network_acl_rule" "allow_ingress_ephemeral" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 400
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = var.vpc_subnet
  from_port      = 1024
  to_port        = 65535
}*/

resource "aws_network_acl_rule" "allow_ingress_ssh_everywhere" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 130
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 22
  to_port        = 22
}

resource "aws_network_acl_rule" "allow_ingress_ssh_everywhere_ipv6" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 135
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  ipv6_cidr_block = "::/0"
  from_port      = 22
  to_port        = 22
}

/*resource "aws_network_acl_rule" "block_ingress_rdp" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 900
  egress         = false
  protocol       = "tcp"
  rule_action    = "deny"
  cidr_block     = var.all_cidr_blocks
  from_port      = 3389
  to_port        = 3389
}*/

/*resource "aws_network_acl_rule" "block_ingress_mssql" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 1000
  egress         = false
  protocol       = "tcp"
  rule_action    = "deny"
  cidr_block     = var.all_cidr_blocks
  from_port      = 1433
  to_port        = 1433
}*/

/*resource "aws_network_acl_rule" "block_ingress_mysql" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 1100
  egress         = false
  protocol       = "tcp"
  rule_action    = "deny"
  cidr_block     = var.all_cidr_blocks
  from_port      = 3306
  to_port        = 3306
}*/

/*resource "aws_network_acl_rule" "block_ingress_postgres" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 1200
  egress         = false
  protocol       = "tcp"
  rule_action    = "deny"
  cidr_block     = var.all_cidr_blocks
  from_port      = 5432
  to_port        = 5432
}*/

/*resource "aws_network_acl_rule" "block_ingress_oracle" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 1300
  egress         = false
  protocol       = "tcp"
  rule_action    = "deny"
  cidr_block     = var.all_cidr_blocks
  from_port      = 2483
  to_port        = 2484
}

resource "aws_network_acl_rule" "allow_ingress_ephemeral2" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 1400
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 1024
  to_port        = 65535
}*/

#--------------------------------------------------------------
# Public Subnet Network ACL - Egress Rules
#--------------------------------------------------------------
/*resource "aws_network_acl_rule" "allow_egress_smtps" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 10
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 465
  to_port        = 465
}*/

/*
resource "aws_network_acl_rule" "allow_egress_ephemeral" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 100
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 1024
  to_port        = 65535
}

resource "aws_network_acl_rule" "allow_egress_http" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 200
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 80
  to_port        = 80
}*/

resource "aws_network_acl_rule" "public_allow_egress_all" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 100
  egress         = true
  protocol       = "all"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
}

/*resource "aws_network_acl_rule" "allow_egress_icmp" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 500
  egress         = true
  protocol       = "icmp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = -1
  to_port        = -1
  icmp_type      = -1
  icmp_code      = -1
}*/

/*resource "aws_network_acl_rule" "allow_egress_ntp" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 550
  egress         = true
  protocol       = "udp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 123
  to_port        = 123
}*/

/*resource "aws_network_acl_rule" "allow_egress_ntp_ephemeral" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 560
  egress         = true
  protocol       = "udp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 1024
  to_port        = 65535
}*/

/*resource "aws_network_acl_rule" "allow_egress_ssh_ftp_all" {
  network_acl_id = aws_network_acl.public.id
  rule_number    = 700
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = var.all_cidr_blocks
  from_port      = 21
  to_port        = 22
}*/
