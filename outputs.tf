output "nat_eips" {
  value = join(",", aws_eip.nat_eip.*.public_ip)
}

output "inbound_http_security_group" {
  value = aws_security_group.http_sg.id
}

output "inbound_https_security_group" {
  value = aws_security_group.https_sg.id
}

output "phz_zone_id" {
  value = aws_route53_zone.vpc_private_zone.zone_id
}

/*output "phz_zone_name" {
  value = var.global_phz_domain
}*/

output "app_route_table" {
  value = join(",", aws_route_table.app_route_table.*.id)
}

output "data_route_table" {
  value = join(",", aws_route_table.data_route_table.*.id)
}

output "public_route_table" {
  value = join(",", aws_route_table.public_route_table.*.id)
}

output "public_subnets" {
  value = join(",", aws_subnet.public_subnets.*.id)
}

output "app_subnets" {
  value = join(",", aws_subnet.app_subnets.*.id)
}

output "data_subnets" {
  value = join(",", aws_subnet.data_subnets.*.id)
}

output "vpc_id" {
  value = aws_vpc.app_vpc.id
}

output "vpc_cidr_block" {
  value = aws_vpc.app_vpc.cidr_block
}
