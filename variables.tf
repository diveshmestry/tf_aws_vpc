variable "all_cidr_blocks" {
  description = "Entire CIDR block range"
  default     = "0.0.0.0/0"
}

variable "alternate_resolver_names" {
  description = "The alternate domain name to be added to /etc/resolv"
  default     = "infra"
}

variable "app_subnets" {
  description = "The CIDR blocks to be used for the private subnets"
  type = list(string)
}

variable "data_subnets" {
  description = "The CIDR blocks to be used for the data subnets"
  type = list(string)
}

variable "NACL_cidr_blocks" {
  description = "List of CIDR blocks for NACLs to use"
  default = ["10.10.0.0/16"]
}

variable "private_hosted_zone_domain" {
  description = "The private hosted zone domain name"
  default     = "blog.vpc.local"
}

variable "availability_zones" {
  description = "The AZs that the VPC will use"
  type        = list(string)
}

variable "public_subnets" {
  description = "The CIDR blocks to be used for the public subnets"
  type = list(string)
}

variable "vpc_contact_tag" {
  description = "Who should be contacted with any queries relating to this VPC. This should be a valid email address"
}

variable "vpc_environment_tag" {
  description = "The environment this VPC is for"
}

variable "vpc_name" {
  description = "The name of the VPC being created"
}

variable "vpc_product_tag" {
  description = "The product this VPC is for"
}

variable "vpc_subnet" {
  description = "The subnet of the VPC being created in CIDR format"
}

variable "vpc_virtual_private_gateway" {
  description = "The virtual private gateway id to propagate through the routing tables"
  default     = ""
}
