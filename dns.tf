#--------------------------------------------------------------
# Private Hosted Forward Lookup Zone
#--------------------------------------------------------------
resource "aws_route53_zone" "vpc_private_zone" {
  name   = var.private_hosted_zone_domain

  vpc {
    vpc_id = aws_vpc.app_vpc.id
  }

  tags = {
    environment = var.vpc_environment_tag
    product     = var.vpc_product_tag
    contact     = var.vpc_contact_tag
  }
}

#--------------------------------------------------------------
# Private Hosted Reverse Lookup Zone
#--------------------------------------------------------------
resource "aws_route53_zone" "reverse_lookup_zone" {
  name   = "${element(split(".", var.vpc_subnet),1)}.${element(split(".", var.vpc_subnet),0)}.in-addr.arpa"

  vpc {
    vpc_id = aws_vpc.app_vpc.id
  }

  tags = {
    Name        = "${element(split(".", var.vpc_subnet),1)}.${element(split(".", var.vpc_subnet),0)}.in-addr.arpa"
    environment = var.vpc_environment_tag
    product     = var.vpc_product_tag
    contact     = var.vpc_contact_tag
  }
}

#--------------------------------------------------------------
# Custom DHCP Option Set
#--------------------------------------------------------------
resource "aws_vpc_dhcp_options" "tio_options" {
  domain_name         = "${var.private_hosted_zone_domain}  ${var.alternate_resolver_names}"
  domain_name_servers = ["AmazonProvidedDNS"]

  tags = {
    Name        = "tio_dhcp_options"
    environment = var.vpc_environment_tag
    product     = var.vpc_product_tag
    contact     = var.vpc_contact_tag
  }
}

#--------------------------------------------------------------
# Association Between VPC and Custom DHCP Option Set
#--------------------------------------------------------------
resource "aws_vpc_dhcp_options_association" "local_resolver" {
  vpc_id          = aws_vpc.app_vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.tio_options.id
}
