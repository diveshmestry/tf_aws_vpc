#--------------------------------------------------------------
# Managed NAT Gateways
#--------------------------------------------------------------
resource "aws_eip" "nat_eip" {
  count = length(var.public_subnets)
  vpc   = true
}

resource "aws_nat_gateway" "nat" {
  count         = length(var.public_subnets)
  allocation_id = element(aws_eip.nat_eip.*.id, count.index)
  subnet_id     = element(aws_subnet.public_subnets.*.id, count.index)
  depends_on    = [aws_internet_gateway.gw]
}
